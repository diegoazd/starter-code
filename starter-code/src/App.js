import React, { Component } from 'react';

import Sidebar from "./components/Sidebar/Sidebar";
import Home from "./components/Home/Home";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Sidebar />
        <Home />
      </div>
    );
  }
}

export default App;
