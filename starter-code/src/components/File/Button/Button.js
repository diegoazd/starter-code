import React, { Component } from 'react';

class Button extends Component {
  favorites = () => {
    this.props.handleClick(this.props.favorite);
  }

  render() {
    return (
      <button className="file__button" onClick={this.favorites} >
        <i className="fa fa-download"></i>
      </button>
    );
  }
}

export default Button;