import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as moment from 'moment';
import Button from './Button/Button'

const icons = {
    word: "fa fa-file-word-o",
    code: "fa fa-code",
    image: "fa fa-file-image-o",
    pdf: "fa fa-file-pdf-o",
    video: "fa fa-file-video-o",
    powerpoint: "fa fa-file-powerpoint-o",
    audio: "fa fa-file-audio-o",
    excel: "fa fa-file-excel-o",
    text: "fa fa-file-text-o",
    zip: "fa fa-file-archive-o",
    default: "fa fa-file-text-o"
}

class File extends Component {
  getIcon(name) {
    return icons[name];
  }

  getDate(date) {
    return moment.unix(date).fromNow();
  }

  render() {
    return (
      <li className="file__item">
        <div className="grid grid--expanded">
          <div className="grid">
            <span className="file__icon">
              <i className={ this.getIcon(this.props.type) }></i>
            </span>
            <p className="file__meta">
              <span className="file__name">{this.props.name}</span> <br />
              <span>Added { this.getDate(this.props.addedAt) } · {this.props.category}</span>
            </p>
          </div>

          <Button handleClick={this.props.handleClick} favorite={this.props}/>

        </div>
      </li>

    );
  }
}

File.defaultProps = {
  name: "default"
}

File.propTypes = {
  name:  PropTypes.string.isRequired
}


export default File;