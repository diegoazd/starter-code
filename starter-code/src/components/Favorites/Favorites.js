import React, { Component, Fragment } from 'react';
import Favorite from './Favorite';

class Favorites extends Component {

  render() {
    return (
        <aside className="favorites">
          <h5>Compress</h5>
          <h6>TOTAL: { this.props.size } KB</h6>
          <ul className="favs">
             { this.props.compress.map( (e,index) => <Favorite name={e.name} key={ `favorites-${index}` }/>) }
          </ul>
        </aside>
    );
  }
}

export default Favorites;