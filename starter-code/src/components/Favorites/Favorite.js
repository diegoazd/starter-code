import React, { Component, Fragment } from 'react';

class Favorite extends Component {
  render() {
    return(
      <li>
        {this.props.name}
      </li>
    );
  }
}

export default Favorite;