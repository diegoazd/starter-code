import React, { Component, Fragment } from "react";

import files from "../files.json";
import "./Home.css";
import File from '../File/File';
import Favorite from '../Favorites/Favorite';
import Favorites from '../Favorites/Favorites';

class Home extends Component {
  constructor() {
    super();
    this.state = {results: files.results, compress: [], size: 0};
  }

  fileFilter = e => {
    const filteredResults = files.results.filter( value => {
      return value.name.toLowerCase().includes(e.target.value.toLowerCase()); 
    });
    this.setState({results : filteredResults});
  }

  handleClick = e => {
    const sumSize = this.state.compress.map( e => this.getKbSize(e.size.split(" ")))
    .reduce( (current, before ) => {
      return current + before;
    },0);
    this.setState({compress: [...this.state.compress, e], size: sumSize});
    this.sumSize();
  }

  sumSize() {
    const sumSize = this.state.compress.map( e => this.getKbSize(e.size.split(" ")))
    .reduce( (current, before ) => {
      return current + before;
    },0);
    console.log(this.state.compress);
    this.setState({size: sumSize});
  }

  getKbSize(value) {
    return value[1] === "MB" ? Number(value[0]) * 1000 : Number(value[0]); 
  }

  render() {
    return (
      <Fragment>
        <main className="home">
          <input className="search" type="search" placeholder="Search" onKeyUp={this.fileFilter}/>
          <h2 className="home__title">Recent {this.state.results.length} files found</h2>
          <ul className="file">
            { this.state.results.map( e => <File key={e.id} name={e.name} addedAt={e.added_at} category={e.category} type={e.type} size={e.size} handleClick={this.handleClick}/>) }
          </ul>
        </main>
        
        <Favorites compress={this.state.compress} size= { this.state.size }/>
        
      </Fragment>
    );
  }
}

export default Home;
